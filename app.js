if(process.env.NODE_ENV === "dev"){
  require('dotenv').config()
}

const TelegramBot = require('node-telegram-bot-api');
const fetch = require('node-fetch');
const TOKEN = process.env.TOKEN;
const api = 'https://manejador-btc.herokuapp.com/api/totalizar';

const options = {
  webHook: {
    // Port to which you should bind is assigned to $PORT variable
    // See: https://devcenter.heroku.com/articles/dynos#local-environment-variables
    port: process.env.PORT
    // you do NOT need to set up certificates since Heroku provides
    // the SSL certs already (https://<app-name>.herokuapp.com)
    // Also no need to pass IP because on Heroku you need to bind to 0.0.0.0
  }
};

if (process.env.NODE_ENV === 'production') {
  url = process.env.APP_URL || 'https://btc-telegram.herokuapp.com:443';
  bot = new TelegramBot(TOKEN, options);
  bot.setWebHook(`${url}/bot${TOKEN}`);
  setTimeout(()=>{
    bot.setWebHook(`${url}/bot${TOKEN}`);
  }, 6*(60*60*1000));
} else {
  bot = new TelegramBot(TOKEN, {
    polling: true
  });
}
bot.onText(/\/start/, (msg) => {
  bot.sendMessage(msg.chat.id, "¡Bienvenido!");
});
bot.onText(/\/precio/, (msg) => {
  bot.sendMessage(msg.chat.id, "Consultando...")
  var consulta = fetch(api).then((response) => response.json())
  consulta.then(res => {
    bot.sendMessage(msg.chat.id, `VENTA: ${res.venta.COP} \n` +
      `COMPRA ${res.compra.COP}\n`);
  });
});
bot.onText(/\/anuncios/, (msg) => {
  var consulta = fetch(api).then((response) => response.json())
  consulta.then(res => {
    bot.sendMessage(msg.chat.id, `VENTA \n` +
      `Precio: ${res.venta.COP} \n` +
      `Link: ${res.venta.linkAnuncio} \n` +
      `COMPRA \n` +
      `Precio: ${res.compra.COP} \n` +
      `Link: ${res.compra.linkAnuncio} \n`);
  });
});
bot.onText(/\/prueba/, (msg) => {
  bot.sendSticker(msg.chat.id, "CAACAgEAAxkBAAP7YAM9rmBe__aNDnd8Agbs5OnN8jwAAr8AA9PQ6RBV00Ggf1NSnh4E")
  .then((r)=>{
    bot.sendMessage(msg.chat.id, `¡Hola!, estoy online, ${process.env.NODE_ENV} :)`)
  });
});

bot.onText(/\/getFileId/, (msg) => {
  //.split(/ +/)
  bot.sendMessage(msg.chat.id, `Envia lo que quieras saber su file_id, aun no anda funcionando :()`);
});